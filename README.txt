A RESTful API for performing money transactions between accounts. Supported
operations:
- Creating an account.
- Inspecting account's details, including balance.
- Transfering money between accounts, potentially involving currency exchange.
- Inspecting account's transaction history.

The implementation is simple, and at times inefficient.

Building and running:
$ mvn compile
$ mvn exec:java -Dexec.mainClass="com.jonasz.Main"
Now the server is running. You can issue sample queries like so:
$ cd src/end2end_test
$ python -m pip install requests simplejson  # ensure .py deps are met
$ python basic.py
See also other client scripts in the same directory.


Dependencies:
- com.fasterxml.jackson.core:jackson-databind:2.97  - for json serialization
- com.sparkjava:spark-core:2.8.0  - http framework

Potential TODOs:
- Persistent implementation of DatabaseClient
- Real implementation of ExchangeRateProvider.
- Exposing exchange rates to the client.
- When creating an account, specify the allowed overdraft.
- Provide more details when looking at account's history.
- Close an account? Wipe out all user's personal information.
- More detailed error reporting.
- And more :)

