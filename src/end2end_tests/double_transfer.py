# If the user mistakenly issues the request twice, only the first one will suceed.
# See the bottom for an example run.

import requests
import simplejson

def get(subpage, params=None):
  print
  print
  print 'GET', subpage, params
  subpage = subpage.strip('/')
  url = 'http://localhost:4567/' + subpage
  r = requests.get(url, params=params)
  r.close()
  print 'response:'
  print r.content
  return simplejson.loads(r.content)


def post(subpage, json=None):
  print
  print
  print 'POST', subpage, json
  subpage = subpage.strip('/')
  r = requests.post('http://localhost:4567/' + subpage, json=json)
  r.close()
  print 'response:'
  print r.content
  return simplejson.loads(r.content)


if __name__ == '__main__':
  create1 = post('account', json=dict(
    firstName='Samuel',
    lastName='Hamilton',
    address='Salinas Valley',
    balance=dict(amountInUnits='123.44', currencyIsoCode='USD'),
  ))

  create2 = post('account', json=dict(
    firstName='Tom',
    lastName='Joad',
    address='Hooverville',
    balance=dict(amountInUnits='103000.44', currencyIsoCode='RUB'),
  ))

  account1 = get('account', params={'iban': create1['accountId']['iban']})

  def issue():
    transfer = post('transfer', json=dict(
      sender=create1['accountId'],
      receiver=create2['accountId'],
      senderFingerprint=account1['fingerprint'],
      amount=dict(amountInUnits='10.05', currencyIsoCode='USD'),
      minConvertedAmount=dict(amountInUnits='10.05', currencyIsoCode='RUB'),
    ))
    return transfer['success']

  assert issue() == True
  assert issue() == False

  print
  print 'SUCCESS'

# Example out:
#
# $ python double_transfer.py
# POST account {'lastName': 'Hamilton', 'balance': {'currencyIsoCode': 'USD', 'amountInUnits': '123.44'}, 'firstName': 'Samuel', 'address': 'Salinas Valley'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "accountId" : {
#     "iban" : "0969934074762284069779"
#   }
# }
# 
# 
# POST account {'lastName': 'Joad', 'balance': {'currencyIsoCode': 'RUB', 'amountInUnits': '103000.44'}, 'firstName': 'Tom', 'address': 'Hooverville'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "accountId" : {
#     "iban" : "8681002956599052232567"
#   }
# }
# 
# 
# GET account {'iban': '0969934074762284069779'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "firstName" : "Samuel",
#   "lastName" : "Hamilton",
#   "address" : "Salinas Valley",
#   "currency" : "USD",
#   "balance" : "123.44",
#   "fingerprint" : "fgpt.-1467071441"
# }
# 
# 
# POST transfer {'minConvertedAmount': {'currencyIsoCode': 'RUB', 'amountInUnits': '10.05'}, 'amount': {'currencyIsoCode': 'USD', 'amountInUnits': '10.05'}, 'senderFingerprint': 'fgpt.-1467071441', 'sender': {'iban': '0969934074762284069779'}, 'receiver': {'iban': '8681002956599052232567'}}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "amountReceived" : "RUB",
#   "currencyReceived" : "664.81"
# }
# 
# 
# POST transfer {'minConvertedAmount': {'currencyIsoCode': 'RUB', 'amountInUnits': '10.05'}, 'amount': {'currencyIsoCode': 'USD', 'amountInUnits': '10.05'}, 'senderFingerprint': 'fgpt.-1467071441', 'sender': {'iban': '0969934074762284069779'}, 'receiver': {'iban': '8681002956599052232567'}}
# response:
# {
#   "success" : false,
#   "errorMessage" : "Fingerprint mismatch.",
#   "amountReceived" : null,
#   "currencyReceived" : null
# }

