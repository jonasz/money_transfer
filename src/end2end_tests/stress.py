# Example run:
#
# $ python stress.py
# Initially, balances are:
# ['100', '100', '100', '100', '100'] -> 500.0
# 
# After 1000 random transfers on 5 concurrent threads, balances are:
# ['2982.5', '-574.11', '-7242.13', '2155.08', '3178.66'] -> 500.0


import requests
import simplejson
import random
from multiprocessing import Pool


def get(subpage, params=None):
  url = 'http://localhost:4567/' + subpage.strip('/')
  r = requests.get(url, params=params)
  r.close()
  return simplejson.loads(r.content)


def post(subpage, json=None):
  r = requests.post('http://localhost:4567/' + subpage.strip('/'), json=json)
  r.close()
  return simplejson.loads(r.content)


if __name__ == '__main__':
  accounts = [
    post('account', json=dict(
      firstName='name_%d' % i,
      lastName='last_name_%d' % i,
      address='city_%d' % i,
      balance=dict(currencyIsoCode='USD', amountInUnits='100'),
    ))
    for i in range(5)
  ]
  accountIds = [a['accountId'] for a in accounts]

  def checkBalance():
    balances = [
      get('account', params={'iban': i['iban']})['balance'] for i in accountIds]
    total = sum([float(x) for x in balances])
    print balances, '->', total
    assert abs(total - 500.) < 1e-7

  def randomTransfer(unused):
    sender, receiver = random.sample(accountIds, 2)
    amount = dict(
      currencyIsoCode='USD',
      amountInUnits=str(round(random.random()*100, 2)),
    )
    transfer = post('transfer', json=dict(
      sender=sender, receiver=receiver, amount=amount,
      senderFingerprint=None,
      minConvertedAmount=dict(currencyIsoCode='USD', amountInUnits='0'),
    ))

  print 'Initially, balances are:'
  checkBalance()
  print

  p = Pool(5)
  p.map(randomTransfer, range(10000))

  print 'After 1000 random transfers on 5 concurrent threads, balances are:'
  checkBalance()

  print
  print 'SUCCESS'
