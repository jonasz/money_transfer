# See the bottom for an example run.

import requests
import simplejson

def get(subpage, params=None):
  print
  print
  print 'GET', subpage, params
  subpage = subpage.strip('/')
  url = 'http://localhost:4567/' + subpage
  r = requests.get(url, params=params)
  r.close()
  print 'response:'
  print r.content
  return simplejson.loads(r.content)


def post(subpage, json=None):
  print
  print
  print 'POST', subpage, json
  subpage = subpage.strip('/')
  r = requests.post('http://localhost:4567/' + subpage, json=json)
  r.close()
  print 'response:'
  print r.content
  return simplejson.loads(r.content)


if __name__ == '__main__':
  create1 = post('account', json=dict(
    firstName='Samuel',
    lastName='Hamilton',
    address='Salinas Valley',
    balance=dict(amountInUnits='123.44', currencyIsoCode='USD'),
  ))

  create2 = post('account', json=dict(
    firstName='Tom',
    lastName='Joad',
    address='Hooverville',
    balance=dict(amountInUnits='103000.44', currencyIsoCode='RUB'),
  ))

  account1 = get('account', params={'iban': create1['accountId']['iban']})

  transfer = post('transfer', json=dict(
    sender=create1['accountId'],
    receiver=create2['accountId'],
    senderFingerprint=account1['fingerprint'],
    amount=dict(amountInUnits='10.05', currencyIsoCode='USD'),
    minConvertedAmount=dict(amountInUnits='10.05', currencyIsoCode='RUB'),
  ))
  assert transfer['success']

  history = get('history', params={'iban': create1['accountId']['iban']})

  account1 = get('account', params={'iban': create1['accountId']['iban']})
  account2 = get('account', params={'iban': create2['accountId']['iban']})
  assert account1['balance'] == str(123.44 - 10.05)
  assert float(account2['balance']) >= 103000.44 + 10.05

  print
  print 'SUCCESS'

# Example out:
# $ python basic.py
# POST account {'lastName': 'Hamilton', 'balance': {'currencyIsoCode': 'USD', 'amountInUnits': '123.44'}, 'firstName': 'Samuel', 'address': 'Salinas Valley'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "accountId" : {
#     "iban" : "1869963651636388945669"
#   }
# }
# 
# 
# POST account {'lastName': 'Joad', 'balance': {'currencyIsoCode': 'RUB', 'amountInUnits': '103000.44'}, 'firstName': 'Tom', 'address': 'Hooverville'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "accountId" : {
#     "iban" : "0992827930177039898746"
#   }
# }
# 
# 
# GET account {'iban': '1869963651636388945669'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "firstName" : "Samuel",
#   "lastName" : "Hamilton",
#   "address" : "Salinas Valley",
#   "currency" : "USD",
#   "balance" : "123.44",
#   "fingerprint" : "fgpt.1632120124"
# }
# 
# 
# POST transfer {'minConvertedAmount': {'currencyIsoCode': 'RUB', 'amountInUnits': '10.05'}, 'amount': {'currencyIsoCode': 'USD', 'amountInUnits': '10.05'}, 'senderFingerprint': 'fgpt.1632120124', 'sender': {'iban': '1869963651636388945669'}, 'receiver': {'iban': '0992827930177039898746'}}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "amountReceived" : "RUB",
#   "currencyReceived" : "664.81"
# }
# 
# 
# GET history {'iban': '1869963651636388945669'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "history" : [ {
#     "sender" : {
#       "iban" : "1869963651636388945669"
#     },
#     "receiver" : {
#       "iban" : "0992827930177039898746"
#     },
#     "amountSent" : {
#       "currencyIsoCode" : "USD",
#       "amountInUnits" : "10.05"
#     },
#     "amountReceived" : {
#       "currencyIsoCode" : "RUB",
#       "amountInUnits" : "664.81"
#     },
#     "timestamp" : 1541330570452
#   } ]
# }
# 
# 
# GET account {'iban': '1869963651636388945669'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "firstName" : "Samuel",
#   "lastName" : "Hamilton",
#   "address" : "Salinas Valley",
#   "currency" : "USD",
#   "balance" : "113.39",
#   "fingerprint" : "fgpt.-376126580"
# }
# 
# 
# GET account {'iban': '0992827930177039898746'}
# response:
# {
#   "success" : true,
#   "errorMessage" : null,
#   "firstName" : "Tom",
#   "lastName" : "Joad",
#   "address" : "Hooverville",
#   "currency" : "RUB",
#   "balance" : "103665.25",
#   "fingerprint" : "fgpt.-376126580"
# }

