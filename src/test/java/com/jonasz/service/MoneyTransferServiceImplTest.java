package com.jonasz.service;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.datamodel.*;
import com.jonasz.exceptions.MoneyTransferException;
import com.jonasz.exchangerate.ExchangeRateProvider;
import com.jonasz.internalaccounting.InternalAccountingProvider;
import com.jonasz.exchangerate.ExchangeRateProviderDummyImpl;
import com.jonasz.internalaccounting.InternalAccountingProviderImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class MoneyTransferServiceImplTest {
    private MoneyTransferService moneyTransferService;

    @Before
    public void setUp() throws MoneyTransferException {
        DatabaseClient database = InMemoryDatabaseClient.getSingleton();
        InternalAccountingProviderImpl.InitializeSingleton(database);
        InternalAccountingProvider internalAccountingProvider = InternalAccountingProviderImpl.getSingleton();
        ExchangeRateProvider exchangeRateProvider = ExchangeRateProviderDummyImpl.getSingleton();
        moneyTransferService = new MoneyTransferServiceImpl(database, internalAccountingProvider, exchangeRateProvider);
    }

    private AccountId createAccount1() throws MoneyTransferException {
        CreateAccountRequest request = new CreateAccountRequest(
                "John", "Steinbeck", "Times Square 1, NYC",
                CurrencyAmount.createExact("USD", "12.50"));
        CreateAccountResponse response = moneyTransferService.createAccount(request);
        assertNotNull(response);
        assertNotNull(response.accountId);
        return response.accountId;
    }

    private AccountId createAccount2() throws MoneyTransferException {
        CreateAccountRequest request = new CreateAccountRequest(
                "Fyodor", "Dostoyevsky", "Kremlin 1, Moscow",
                CurrencyAmount.createExact("RUB", "214.52"));
        CreateAccountResponse response = moneyTransferService.createAccount(request);
        assertNotNull(response);
        assertNotNull(response.accountId);
        return response.accountId;
    }

    @Test
    public void createAndRetrieveAccount() throws MoneyTransferException {
        AccountId accountId = createAccount1();
        assertEquals(22, accountId.getIban().length());

        GetAccountResponse res = moneyTransferService.getAccount(new GetAccountRequest(accountId));
        assertNotNull(res);
        assertEquals("John", res.firstName);
        assertEquals("Steinbeck", res.lastName);
        assertEquals("Times Square 1, NYC", res.address);
    }

    @Test
    public void createTwoAccounts() throws MoneyTransferException {
        AccountId accountId1 = createAccount1();
        AccountId accountId2 = createAccount2();

        assertNotEquals(accountId1, accountId2);

        GetAccountResponse res1 = moneyTransferService.getAccount(new GetAccountRequest(accountId1));
        GetAccountResponse res2 = moneyTransferService.getAccount(new GetAccountRequest(accountId2));
        assertEquals("John", res1.firstName);
        assertEquals("Fyodor", res2.firstName);
    }

    @Test
    public void getNonExistentAccount() throws MoneyTransferException {
        createAccount1();
        GetAccountResponse response = moneyTransferService.getAccount(
                new GetAccountRequest(new AccountId("nosuchiban")));
        assertFalse(response.success);
    }

    @Test
    public void transferMoney() throws MoneyTransferException {
        AccountId accountId1 = createAccount1();
        AccountId accountId2 = createAccount2();

        TransferMoneyRequest request = new TransferMoneyRequest(
                accountId1, accountId2, null,
                CurrencyAmount.createExact("USD", "1.05"),
                CurrencyAmount.createExact("RUB","0.0"));
        TransferMoneyResponse response = moneyTransferService.transferMoney(request);
        assertTrue(response.success);
    }

    @Test
    public void transferMoneyNegativeAmount() throws MoneyTransferException {
        AccountId accountId1 = createAccount1();
        AccountId accountId2 = createAccount2();

        TransferMoneyRequest request = new TransferMoneyRequest(
                accountId1,
                accountId2,
                null,
                CurrencyAmount.createExact("USD", "-1.05"),
                CurrencyAmount.createExact("RUB", "-2000.0"));
        TransferMoneyResponse response = moneyTransferService.transferMoney(request);
        assertFalse(response.success);
    }

    @Test
    public void transferMoneyDoubleRequest() throws MoneyTransferException {
        AccountId accountId1 = createAccount1();
        AccountId accountId2 = createAccount2();
        GetAccountResponse account1 = moneyTransferService.getAccount(new GetAccountRequest(accountId1));

        TransferMoneyRequest request = new TransferMoneyRequest(
                accountId1,
                accountId2,
                account1.fingerprint,
                CurrencyAmount.createExact("USD", "1.05"),
                CurrencyAmount.createExact("RUB", ".0"));

        TransferMoneyResponse response1 = moneyTransferService.transferMoney(request);
        TransferMoneyResponse response2 = moneyTransferService.transferMoney(request);

        assertTrue(response1.success);
        assertFalse(response2.success);
    }

    @Test
    public void transferMoneyInsufficientExchangeRate() throws MoneyTransferException {
        DatabaseClient database = InMemoryDatabaseClient.getSingleton();
        InternalAccountingProviderImpl.InitializeSingleton(database);
        InternalAccountingProvider internalAccountingProvider = InternalAccountingProviderImpl.getSingleton();
        ExchangeRateProvider exchangeRateProvider = new ExchangeRateProvider() {
            @Override
            public BigDecimal getExchangeRate(String currencyIsoCodeFrom, String currencyIsoCodeTo) {
                return new BigDecimal("2.0");
            }
        };
        moneyTransferService = new MoneyTransferServiceImpl(database, internalAccountingProvider, exchangeRateProvider);

        AccountId accountId1 = createAccount1();
        AccountId accountId2 = createAccount2();
        GetAccountResponse account1 = moneyTransferService.getAccount(new GetAccountRequest(accountId1));

        // The exchange rate of 2.0 only guarantees 2.1 RUB, but the request specifies the minimum amount to be
        // more than that.
        TransferMoneyRequest request = new TransferMoneyRequest(
                accountId1,
                accountId2,
                account1.fingerprint,
                CurrencyAmount.createExact("USD", "1.05"),
                CurrencyAmount.createExact("RUB", "2.11"));

        TransferMoneyResponse response = moneyTransferService.transferMoney(request);
        assertFalse(response.success);
    }

    @Test
    public void getHistory() throws MoneyTransferException {
        AccountId accountId1 = createAccount1();
        AccountId accountId2 = createAccount2();

        moneyTransferService.transferMoney(new TransferMoneyRequest(
                accountId1, accountId2, null,
                CurrencyAmount.createExact("USD", "1.05"),
                CurrencyAmount.createExact("RUB", "0.0")));
        AccountHistoryResponse response1 = moneyTransferService.getAccountHistory(new AccountHistoryRequest(accountId1));
        assertEquals(1, response1.history.size());

        moneyTransferService.transferMoney(new TransferMoneyRequest(
                accountId2, accountId1, null,
                CurrencyAmount.createExact("RUB", "3.05"),
                CurrencyAmount.createExact("USD", "0.00")));
        AccountHistoryResponse response2 = moneyTransferService.getAccountHistory(new AccountHistoryRequest(accountId1));
        assertEquals(2, response2.history.size());
    }
}