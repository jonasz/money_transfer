package com.jonasz.internalaccounting;

import com.jonasz.datamodel.AccountId;
import com.jonasz.datamodel.DatabaseClient;
import com.jonasz.datamodel.InMemoryDatabaseClient;
import com.jonasz.exceptions.MoneyTransferException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class InternalAccountingProviderImplTest {
    private static InternalAccountingProvider provider;

    @BeforeClass
    public static void setUpClass() throws MoneyTransferException {
        DatabaseClient database = InMemoryDatabaseClient.getSingleton();
        InternalAccountingProviderImpl.InitializeSingleton(database);
        provider = InternalAccountingProviderImpl.getSingleton();
    }

    @Test
    public void getInternalAccountConsistent() {
        AccountId chf1 = provider.getInternalAccount("CHF");
        AccountId chf2 = provider.getInternalAccount("CHF");
        assertEquals(chf1, chf2);
    }

    @Test
    public void getInternalAccountKnownCurrencies() {
        assertNotNull(provider.getInternalAccount("CHF"));
        assertNotNull(provider.getInternalAccount("USD"));
        assertNotNull(provider.getInternalAccount("RUB"));
        assertNotNull(provider.getInternalAccount("CZK"));
    }

    @Test
    public void getInternalAccountMissing() {
        assertNull(provider.getInternalAccount("XYZ"));
    }
}