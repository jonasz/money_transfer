package com.jonasz.exchangerate;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.exceptions.ExchangeRateUnknown;
import com.jonasz.exceptions.MoneyTransferException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ExchangeRateProviderDummyImplTest {
    private final ExchangeRateProvider provider = ExchangeRateProviderDummyImpl.getSingleton();

    @Test
    public void getExchangeRate() throws ExchangeRateUnknown {
        BigDecimal rate = provider.getExchangeRate("CHF", "USD");
        assertEquals(1, rate.compareTo(BigDecimal.ZERO));
    }

    @Test
    public void convertCurrency() throws MoneyTransferException {
        CurrencyAmount amount = provider.convertCurrency(CurrencyAmount.createExact("USD", "10"), "RUB");
        assertEquals("RUB", amount.getCurrencyIsoCode());
        assertEquals(1, amount.inUnits().compareTo(BigDecimal.ZERO));
    }

    @Test(expected = ExchangeRateUnknown.class)
    public void convertCurrencyUnknownRate() throws MoneyTransferException {
        CurrencyAmount amount = provider.convertCurrency(CurrencyAmount.createExact("USD", "10"), "CZK");
    }
}