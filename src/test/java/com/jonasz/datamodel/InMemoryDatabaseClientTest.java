package com.jonasz.datamodel;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.exceptions.MoneyTransferException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class InMemoryDatabaseClientTest {
    private final DatabaseClient client = InMemoryDatabaseClient.getSingleton();

    private Account createAccount1() throws MoneyTransferException {
        AccountBuilder builder = new AccountBuilder();
        builder.setBalance(CurrencyAmount.createExact("USD", "10."));
        builder.setFingerprint("abc");
        builder.setFirstName("John");
        builder.setLastName("Andrews");
        return client.createAccount(builder);
    }

    private Account createAccount2() throws MoneyTransferException {
        AccountBuilder builder = new AccountBuilder();
        builder.setBalance(CurrencyAmount.createExact("RUB", "20."));
        builder.setFingerprint("def");
        builder.setFirstName("Dmitry");
        builder.setLastName("Ivanov");
        builder.setAddress("Moscow");
        return client.createAccount(builder);
    }

    @Test
    public void createAndGetAccount() throws MoneyTransferException {
        Account account = createAccount1();
        createAccount2();
        Account retrieved = client.getAccount(account.id);

        assertEquals(account.fingerprint, retrieved.fingerprint);
        assertEquals(account.address, retrieved.address);
        assertEquals(account.firstName, retrieved.firstName);
        assertEquals(account.lastName, retrieved.lastName);
    }

    @Test
    public void getAccountHistory() throws MoneyTransferException {
        Account account1 = createAccount1();
        Account account2 = createAccount2();

        TransactionBuilder builder = new TransactionBuilder();
        builder.setReceiver(account1.id);
        builder.setSender(account2.id);
        builder.setAmountSent(CurrencyAmount.createExact("USD", "10."));
        builder.setAmountReceived(CurrencyAmount.createExact("RUB", "60."));
        Transaction transaction = client.createTransaction(builder);

        assertTrue(transaction.timestamp > 0);

        List<Transaction> history = client.getAccountHistory(account2.id);

        assertEquals(1, history.size());
        assertEquals(history.get(0).id, transaction.id);
    }
}
