package com.jonasz.datamodel;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class InMemoryTableTest {
    InMemoryTable<Integer, Integer> table = new InMemoryTable<Integer, Integer>() {
        @Override
        protected Integer generateId() {
            return new Random().nextInt(100);
        }
    };

    Integer createRow(Integer value) {
        InMemoryTable<Integer, Integer>.StagedChanges changes = table.new StagedChanges();
        Integer id = changes.newRow();
        changes.lock();
        changes.updateRow(id, value);
        changes.apply();
        changes.release();
        return id;
    }

    @Test
    public void createTwoRows() {
        InMemoryTable<Integer, Integer>.StagedChanges changes = table.new StagedChanges();
        Integer id1 = changes.newRow();
        Integer id2 = changes.newRow();
        changes.lock();
        changes.updateRow(id1, 10);
        changes.updateRow(id2, 20);
        changes.apply();
        changes.release();

        assertEquals(10, table.get(id1).longValue());
        assertEquals(20, table.get(id2).longValue());
    }

    @Test
    public void concurrentUpdates() throws InterruptedException {
        // N threads, each increments the value of a single row M times.
        final int numThreads = 8;
        final int numUpdates = 20;

        Integer id = createRow(0);
        List<Thread> threads = new LinkedList<Thread>();
        for (int i = 0; i < numThreads; i++) {
            Thread t = new Thread (() -> {
                for (int j = 0; j < numUpdates; j++) {
                    InMemoryTable<Integer, Integer>.StagedChanges changes = table.new StagedChanges();
                    changes.addId(id);
                    changes.lock();
                    Integer newVal = changes.get(id) + 1;
                    changes.updateRow(id, newVal);
                    changes.apply();
                    try {
                        Thread.sleep(new Random().nextInt(20));
                    } catch (InterruptedException e) {
                    }
                    changes.release();
                }
            });
            threads.add(t);
            t.start();
        }
        for (Thread t : threads) {
            t.join();
        }
        Integer finalValue = table.get(id);
        assertEquals(numThreads * numUpdates, finalValue.longValue());
    }

    @Test(expected = RuntimeException.class)
    public void updateNonMarkedRow() {
        InMemoryTable<Integer, Integer>.StagedChanges changes = table.new StagedChanges();
        changes.addId(128);
        changes.lock();
        changes.updateRow(125, 200);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithoutLocking() {
        InMemoryTable<Integer, Integer>.StagedChanges changes = table.new StagedChanges();
        Integer id = createRow(200);
        changes.addId(id);
        changes.updateRow(id, 202);
    }
}