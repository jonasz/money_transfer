package com.jonasz.currency;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jonasz.currency.CurrencyAmount;
import com.jonasz.exceptions.MalformedCurrencyAmount;
import com.jonasz.exceptions.MoneyTransferException;
import com.jonasz.exceptions.UnsupportedCurrency;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class CurrencyAmountTest {
    @Test
    public void testCreateExact() throws MoneyTransferException {
        CurrencyAmount c1 = CurrencyAmount.createExact("USD", "2.6000");
        assertEquals("USD 2.6", c1.toString());

        CurrencyAmount c2 = CurrencyAmount.createExact("CZK", "-3.0000000");
        assertEquals("CZK -3", c2.toString());

        CurrencyAmount c3 = CurrencyAmount.createExact("KWD", "-3.123");
        assertEquals("KWD -3.123", c3.toString());
    }

    @Test
    public void testCreateRound() throws MoneyTransferException {
        CurrencyAmount c1 = CurrencyAmount.createRound("USD", "2.60001");
        assertEquals("USD 2.6", c1.toString());

        CurrencyAmount c2 = CurrencyAmount.createRound("CZK", "-3.9");
        assertEquals("CZK -3", c2.toString());

        CurrencyAmount c3 = CurrencyAmount.createRound("KWD", "-3.12395");
        assertEquals("KWD -3.123", c3.toString());
    }

    @Test(expected = MalformedCurrencyAmount.class)
    public void testInvalidAmount() throws MoneyTransferException {
        CurrencyAmount c1 = CurrencyAmount.createExact("USD", "2.605");
    }

    @Test(expected = UnsupportedCurrency.class)
    public void testUnsupportedCurrency() throws MoneyTransferException {
        CurrencyAmount c1 = CurrencyAmount.createExact("ABC", "2.605");
    }

    @Test
    public void roundtripJson () throws MoneyTransferException, IOException {
        CurrencyAmount c1 = CurrencyAmount.createExact("KWD", "-2.605");
        String json = new ObjectMapper().writeValueAsString(c1);
        CurrencyAmount c2 = new ObjectMapper().readValue(json, CurrencyAmount.class);
        assertEquals(c1.toString(), c2.toString());
    }
}