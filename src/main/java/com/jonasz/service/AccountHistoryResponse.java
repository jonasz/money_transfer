package com.jonasz.service;

import com.jonasz.datamodel.Transaction;

import java.util.List;

public class AccountHistoryResponse {
    private final boolean success;
    private final String errorMessage;
    public final List<Transaction> history;

    public static AccountHistoryResponse Fail(String message) {
        return new AccountHistoryResponse(false, message, null);
    }

    public static AccountHistoryResponse Success(List<Transaction>history) {
        return new AccountHistoryResponse(true, null, history);
    }

    private AccountHistoryResponse(boolean success, String errorMessage, List<Transaction> history) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.history = history;
    }
}
