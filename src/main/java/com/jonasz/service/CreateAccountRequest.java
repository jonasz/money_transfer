package com.jonasz.service;

import com.jonasz.currency.CurrencyAmount;

public class CreateAccountRequest {
    public CreateAccountRequest(String firstName, String lastName, String address, CurrencyAmount balance) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.balance = balance;
    }

    public CreateAccountRequest() {}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CurrencyAmount getBalance() { return balance; }

    public void setBalance(CurrencyAmount balance) { this.balance = balance; }

    private String firstName;
    private String lastName;
    private String address;
    private CurrencyAmount balance;
}
