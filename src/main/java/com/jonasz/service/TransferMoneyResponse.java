package com.jonasz.service;

import com.jonasz.currency.CurrencyAmount;

public class TransferMoneyResponse {
    private TransferMoneyResponse(boolean success, String errorMessage, String amountReceived, String currencyReceived) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.amountReceived = amountReceived;
        this.currencyReceived = currencyReceived;
    }

    static public TransferMoneyResponse Fail(String message) {
        return new TransferMoneyResponse(false, message, null, null);
    }

    static public TransferMoneyResponse Success(CurrencyAmount amountReceived) {
        return new TransferMoneyResponse(true, null, amountReceived.getCurrencyIsoCode(),
                amountReceived.inUnits().toString());
    }

    public final boolean success;
    public final String errorMessage;
    private final String amountReceived;
    private final String currencyReceived;
}
