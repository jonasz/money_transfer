package com.jonasz.service;

import com.jonasz.internalaccounting.InternalAccountingProvider;
import com.jonasz.currency.CurrencyAmount;
import com.jonasz.datamodel.*;
import com.jonasz.exceptions.*;
import com.jonasz.exchangerate.ExchangeRateProvider;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class MoneyTransferServiceImpl implements MoneyTransferService {
    public MoneyTransferServiceImpl(DatabaseClient database, InternalAccountingProvider internalAccountingProvider,
                                    ExchangeRateProvider exchangeRateProvider) {
        this.database = database;
        this.internalAccountingProvider = internalAccountingProvider;
        this.exchangeRateProvider = exchangeRateProvider;
    }

    @Override
    public CreateAccountResponse createAccount(CreateAccountRequest createAccountRequest) {
        AccountBuilder accountBuilder = new AccountBuilder();
        accountBuilder.setBalance(createAccountRequest.getBalance());
        accountBuilder.setFirstName(createAccountRequest.getFirstName());
        accountBuilder.setLastName(createAccountRequest.getLastName());
        accountBuilder.setAddress(createAccountRequest.getAddress());
        try {
            Account account = database.createAccount(accountBuilder);
            return CreateAccountResponse.Success(account.id);
        } catch (MoneyTransferException e) {
            return CreateAccountResponse.Fail(e.getMessage());
        }
    }

    @Override
    public GetAccountResponse getAccount(GetAccountRequest getAccountRequest) {
        Account account = database.getAccount(getAccountRequest.getAccountId());
        if (account == null) {
            return GetAccountResponse.Fail("No such account.");
        }
        return GetAccountResponse.Success(account);
    }

    @Override
    public TransferMoneyResponse transferMoney(TransferMoneyRequest transferMoneyRequest) {
        try {
            Account senderAccount = database.getAccount(transferMoneyRequest.getSender());
            Account receiverAccount = database.getAccount(transferMoneyRequest.getReceiver());

            if (transferMoneyRequest.getAmount().inUnits().compareTo(BigDecimal.ZERO) < 0) {
                throw new MoneyTransferException("Cannot transfer a negative amount");
            }

            CurrencyAmount receiverCurrencyAmount = exchangeRateProvider.convertCurrency(
                    transferMoneyRequest.getAmount(), receiverAccount.balance.getCurrencyIsoCode());
            if (transferMoneyRequest.getMinConvertedAmount() != null &&
                    receiverCurrencyAmount.compareTo(transferMoneyRequest.getMinConvertedAmount()) == -1) {
                throw new InsufficientExchangeRate("The converted amount, " + receiverCurrencyAmount +
                        " would be lower than the specified minimum " + transferMoneyRequest.getMinConvertedAmount());
            }
            LinkedList<AccountUpdate> accountUpdates = new LinkedList<>();

            accountUpdates.addLast(new AccountBalanceUpdate(senderAccount.id,
                    transferMoneyRequest.getAmount().negate(), transferMoneyRequest.getSenderFingerprint()));
            accountUpdates.addLast(new AccountBalanceUpdate(receiverAccount.id, receiverCurrencyAmount,
                    null));

            accountUpdates.addLast(new AccountBalanceUpdate(internalAccountingProvider.getInternalAccount(
                    transferMoneyRequest.getAmount().getCurrencyIsoCode()), transferMoneyRequest.getAmount(), null));
            accountUpdates.addLast(new AccountBalanceUpdate(internalAccountingProvider.getInternalAccount(
                    receiverCurrencyAmount.getCurrencyIsoCode()), receiverCurrencyAmount.negate(), null));

            accountUpdates.addLast(new RecordTransaction(senderAccount.id,
                    receiverAccount.id,
                    transferMoneyRequest.getAmount(),
                    receiverCurrencyAmount));
            database.executeAccountUpdatesAtomically(accountUpdates);

            return TransferMoneyResponse.Success(receiverCurrencyAmount);
        } catch (MoneyTransferException e) {
            return TransferMoneyResponse.Fail(e.getMessage());
        }
    }

    @Override
    public AccountHistoryResponse getAccountHistory(AccountHistoryRequest accountHistoryRequest) {
        List<Transaction> history = database.getAccountHistory(accountHistoryRequest.getAccountId());
        return AccountHistoryResponse.Success(history);
    }

    private final DatabaseClient database;
    private final InternalAccountingProvider internalAccountingProvider;
    private final ExchangeRateProvider exchangeRateProvider;
}
