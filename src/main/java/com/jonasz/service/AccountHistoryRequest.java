package com.jonasz.service;

import com.jonasz.datamodel.AccountId;

public class AccountHistoryRequest {
    public AccountHistoryRequest(AccountId accountId) {
        this.accountId = accountId;
    }

    public AccountId getAccountId() {
        return accountId;
    }

    private final AccountId accountId;
}