package com.jonasz.service;


/**
 * For each method, please see the Request object for usage guidelines. Where not self-explanatory, additional
 * documentation is provided there.
 */
public interface MoneyTransferService {
    CreateAccountResponse createAccount(CreateAccountRequest createAccountRequest);
    GetAccountResponse getAccount(GetAccountRequest getAccountRequest);
    TransferMoneyResponse transferMoney(TransferMoneyRequest transfer);
    AccountHistoryResponse getAccountHistory(AccountHistoryRequest accountHistoryRequest);
}
