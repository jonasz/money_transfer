package com.jonasz.service;

import com.jonasz.datamodel.AccountId;

public class CreateAccountResponse {
    private CreateAccountResponse(boolean success, String errorMessage, AccountId accountId) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.accountId = accountId;
    }

    static public CreateAccountResponse Fail(String message) {
        return new CreateAccountResponse(false, message, null);
    }

    static public CreateAccountResponse Success(AccountId accountId) {
        return new CreateAccountResponse(true, null, accountId);
    }

    private final boolean success;
    private final String errorMessage;
    public final AccountId accountId;

}
