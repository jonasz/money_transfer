package com.jonasz.service;

import com.jonasz.datamodel.Account;

public class GetAccountResponse {
    private GetAccountResponse(boolean success, String errorMessage, String firstName, String lastName, String address,
                               String currency, String balance, String fingerprint) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.currency = currency;
        this.balance = balance;
        this.fingerprint = fingerprint;
    }

    public static GetAccountResponse Fail(String message) {
        return new GetAccountResponse(false, message, null, null, null, null,
                null, null);
    }

    public static GetAccountResponse Success(Account account) {
        return new GetAccountResponse(true, null, account.firstName, account.lastName, account.address,
                account.balance.getCurrencyIsoCode(), account.balance.inUnits().toString(), account.fingerprint);
    }

    public final boolean success;
    public final String errorMessage;
    public final String firstName;
    public final String lastName;
    public final String address;
    public final String currency;
    public final String balance;
    public final String fingerprint;
}
