package com.jonasz.service;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.datamodel.AccountId;

/**
 * TransferMoneyRequest may contain senderFingerprint. It is a String that identifies the state of the sender's account
 * at a certain point in time. The transfer is only going to be performed if the current fingerprint of the sender's
 * account matches that specified in the request. It is a simple mechanism that prevents unintentional, uninformed,
 * or double transfer requests.
 * Likewise, the sender issues the request upon inspecting the current conversion rate. Optionally, the sender may
 * indicate the lowest acceptable converted amount. This mechanism shields the sender from performing unfavorable
 * transfers informed by stale information on conversion rate.
 */
public class TransferMoneyRequest {
    public TransferMoneyRequest(AccountId sender, AccountId receiver, String senderFingerprint, CurrencyAmount amount,
                                CurrencyAmount minConvertedAmount) {
        this.sender = sender;
        this.receiver = receiver;
        this.senderFingerprint = senderFingerprint;
        this.amount = amount;
        this.minConvertedAmount = minConvertedAmount;
    }

    public TransferMoneyRequest() {}

    public AccountId getSender() {
        return sender;
    }

    public AccountId getReceiver() {
        return receiver;
    }

    public String getSenderFingerprint() {
        return senderFingerprint;
    }

    public CurrencyAmount getAmount() { return amount; }

    public CurrencyAmount getMinConvertedAmount() { return minConvertedAmount; }

    private AccountId sender;
    private AccountId receiver;
    private String senderFingerprint;
    private CurrencyAmount amount;
    private CurrencyAmount minConvertedAmount;
}
