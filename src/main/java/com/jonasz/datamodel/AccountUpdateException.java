package com.jonasz.datamodel;

import com.jonasz.exceptions.MoneyTransferException;

class AccountUpdateException extends MoneyTransferException {
    public AccountUpdateException(String message) { super(message); }
}
