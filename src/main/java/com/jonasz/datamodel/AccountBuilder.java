package com.jonasz.datamodel;

import com.jonasz.currency.CurrencyAmount;

public class AccountBuilder {
    private AccountId id;
    private CurrencyAmount balance;
    private String firstName;
    private String lastName;
    private String address;
    private String fingerprint;
    private boolean built = false;

    public void setId(AccountId id) {
        this.id = id;
    }

    public AccountId getId() {
        return id;
    }

    public CurrencyAmount getBalance() {
        return balance;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setBalance(CurrencyAmount balance) {
        this.balance = balance;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public AccountBuilder(Account other) {
        setId(other.id);
        setBalance(other.balance);
        setFirstName(other.firstName);
        setLastName(other.lastName);
        setAddress(other.address);
        setFingerprint(other.fingerprint);
    }

    public AccountBuilder() { }

    Account build() {
        if (built) {
            throw new IllegalStateException("The build method was already called.");
        }
        built = true;
        return new Account(this.id, this.balance, this.firstName, this.lastName, this.address, this.fingerprint);
    }
}
