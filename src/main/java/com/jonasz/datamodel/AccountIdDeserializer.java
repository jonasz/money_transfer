package com.jonasz.datamodel;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

/**
 * Implements json -> AccountId conversion.
 */
class AccountIdDeserializer extends StdDeserializer<AccountId> {

    public AccountIdDeserializer() {
        this(null);
    }

    public AccountIdDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public AccountId deserialize(JsonParser jp, DeserializationContext ctx)
            throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String iban = node.get("iban").asText();
        return new AccountId(iban);
    }
}
