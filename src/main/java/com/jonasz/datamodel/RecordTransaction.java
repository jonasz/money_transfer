package com.jonasz.datamodel;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.exceptions.MoneyTransferException;
import com.jonasz.service.MoneyTransferService;

public class RecordTransaction extends AccountUpdate {
    public RecordTransaction(AccountId sender,
                             AccountId receiver,
                             CurrencyAmount amountSent,
                             CurrencyAmount amountReceived) {
        transactionBuilder = new TransactionBuilder();
        transactionBuilder.setSender(sender);
        transactionBuilder.setReceiver(receiver);
        transactionBuilder.setAmountSent(amountSent);
        transactionBuilder.setAmountReceived(amountReceived);
    }

    public RecordTransaction(TransactionBuilder builder) {
        this.transactionBuilder = builder;
    }

    private void updateFingerprint(InMemoryDatabaseClient.DBTransaction dbt, AccountId accountId) throws MoneyTransferException {
        Account account = dbt.getAccount(accountId);
        if (account == null) {
            throw new MoneyTransferException("Recording a transaction for an nonexisting account " + accountId.getIban());
        }
        AccountBuilder builder = new AccountBuilder(account);
        builder.setFingerprint("fgpt." + String.valueOf(builder.getId()).hashCode());
        dbt.updateAccount(builder.build());
    }

    public void fetchRows(InMemoryDatabaseClient.DBTransaction dbt) {
        dbt.fetchAccount(transactionBuilder.getReceiver());
        dbt.fetchAccount(transactionBuilder.getSender());
        transactionBuilder.setId(dbt.reserveTransactionRow());
    }

    public void performUpdate(InMemoryDatabaseClient.DBTransaction dbt) throws MoneyTransferException {
        transactionBuilder.setTimestamp(System.currentTimeMillis());

        transaction = transactionBuilder.build();
        dbt.updateTransaction(transaction);

        updateFingerprint(dbt, transactionBuilder.getSender());
        updateFingerprint(dbt, transactionBuilder.getReceiver());
    }

    public Transaction getTransaction() {
        return transaction;
    }

    private TransactionBuilder transactionBuilder;
    private CurrencyAmount amount;
    private String fingerprint;
    private Transaction transaction;
}
