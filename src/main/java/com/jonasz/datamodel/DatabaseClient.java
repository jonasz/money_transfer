package com.jonasz.datamodel;

import com.jonasz.exceptions.MoneyTransferException;

import java.util.List;

public interface DatabaseClient {
    Account getAccount(AccountId id);
    Transaction getTransaction(long id);

    /**
     * Creates a row in Accounts table.
     * @param accountBuilder Represents the account to create. All fields of the builder, except `id`, should be set.
     * @return The created Account row.
     */
    Account createAccount(AccountBuilder accountBuilder) throws MoneyTransferException;
    /**
     * Creates a row in Transactions table.
     * @param transactionBuilder Represents the transaction to create. All fields of the builder, except `id`, should be set.
     * @return The created Transaction row.
     */
    Transaction createTransaction(TransactionBuilder transactionBuilder) throws MoneyTransferException;

    /**
     * Performs the given accountUpdates in a single transaction.
     * @param accountUpdates Updates to apply. The order of updates is respected.
     * @throws MoneyTransferException Thrown when any of the account updates fail. See specific AccountUpdates for
     * more details.
     */
    void executeAccountUpdatesAtomically(List<AccountUpdate> accountUpdates) throws MoneyTransferException;

    List<Transaction> getAccountHistory(AccountId accountId);
}
