package com.jonasz.datamodel;

import com.jonasz.currency.CurrencyAmount;

/**
 * A Transaction instance represents a single row in the transactions table.
 */
public class Transaction implements Comparable<Transaction> {
    final long id;
    final AccountId sender;
    final AccountId receiver;
    final CurrencyAmount amountSent;
    final CurrencyAmount amountReceived;
    final long timestamp;

    Transaction(long id, AccountId sender, AccountId receiver, CurrencyAmount amountSent, CurrencyAmount amountReceived, long timestamp) {
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.amountSent = amountSent;
        this.amountReceived = amountReceived;
        this.timestamp = timestamp;
    }

    public AccountId getSender() {
        return sender;
    }

    public AccountId getReceiver() {
        return receiver;
    }

    public CurrencyAmount getAmountSent() {
        return amountSent;
    }

    public CurrencyAmount getAmountReceived() {
        return amountReceived;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public int compareTo(Transaction transaction) {
        long res = timestamp - transaction.timestamp;
        if (res < 0) return -1;
        if (res ==0) return 0;
        return 1;
    }
}
