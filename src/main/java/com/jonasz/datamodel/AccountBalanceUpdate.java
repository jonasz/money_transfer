package com.jonasz.datamodel;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.exceptions.MoneyTransferException;

/**
 * Encapsulates an update to an Account, wherein we increase (or decrease) the balance by the specified amount.
 */
public class AccountBalanceUpdate extends AccountUpdate {
    /**
     * AccountBalanceUpdate constructor.
     * @param accountId Identifies the account whose balance is to be updated.
     * @param amount By how much to change the balance. Note: amount has to be in the currency of the account.
     * @param fingerprint Optional - a mechanism to prevent double execution of a single transfer request. The
     *                    fingerprint is a hash of the state of the account. We execute the update only if the
     *                    fingerprint provided by the requester is the same as the current account's fingerprint.
     */
    public AccountBalanceUpdate(AccountId accountId, CurrencyAmount amount, String fingerprint) {
        this.accountId = accountId;
        this.amount = amount;
        this.fingerprint = fingerprint;
    }

    public void performUpdate(InMemoryDatabaseClient.DBTransaction dbt) throws MoneyTransferException {
        Account account = dbt.getAccount(this.accountId);
        AccountBuilder builder = new AccountBuilder(account);

        CurrencyAmount balance = builder.getBalance();
        if (fingerprint != null && !builder.getFingerprint().equals(fingerprint)) {
            throw new MoneyTransferException("Fingerprint mismatch.");
        }
        if (balance == null) {
            throw new MoneyTransferException("Account balance not available.");
        }
        if (!balance.getCurrencyIsoCode().equals(amount.getCurrencyIsoCode())) {
            throw new MoneyTransferException("Currency mismatch: " + balance.getCurrencyIsoCode() + " vs " + amount.getCurrencyIsoCode());
        }
        builder.setBalance(balance.add(amount));

        dbt.updateAccount(builder.build());
    }

    public void fetchRows(InMemoryDatabaseClient.DBTransaction dbt) {
        dbt.fetchAccount(accountId);
    }

    private final AccountId accountId;
    private final CurrencyAmount amount;
    private final String fingerprint;
}
