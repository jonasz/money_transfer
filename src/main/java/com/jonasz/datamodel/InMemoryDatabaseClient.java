package com.jonasz.datamodel;

import com.jonasz.exceptions.MoneyTransferException;

import java.util.*;

/**
 * In-memory implementation of a database client. The InMemoryDatabaseClient is a singleton that holds all database
 * contents in HashMaps. The implementation is just a proof of concept, and very inefficient:
 * - Locking is not granular - we lock the entire database when reading or writing to the database.
 * - We implement transactions by copying the entire database to a new set of HashMaps. Upon exception, we rollback
 *   all modifications by restoring these copies.
 */
public class InMemoryDatabaseClient implements DatabaseClient {
    private static final InMemoryDatabaseClient singleton = new InMemoryDatabaseClient();

    public static InMemoryDatabaseClient getSingleton() {
        return singleton;
    }

    @Override
    public Account getAccount(AccountId id) {
        return accounts.get(id);
    }

    @Override
    public Transaction getTransaction(long id) {
        return transactions.get(id);
    }

    @Override
    public List<Transaction> getAccountHistory(AccountId accountId) {
        // Note: we are using getAllAsync, as we don't want to block the entire transactions table for reading. At the
        // same time, getAllAsync works just fine for us, as transaction entries are never removed nor modified.
        // TODO: optimize. Right now we iterate through the entire table and filter entries for the given user. Of course
        // this can be optimized easily by keeping the transactions in memory per-user.
        return transactions.getAllAsync(transactions.new Filter() {
            boolean predicate(Transaction t) {
                return t.receiver.equals(accountId) || t.sender.equals(accountId);
            }
        });
    }


    @Override
    public void executeAccountUpdatesAtomically(List<AccountUpdate> updates) throws MoneyTransferException {
        DBTransaction dbt = new DBTransaction();
        for (AccountUpdate update : updates) {
            update.fetchRows(dbt);
        }

        try {
            dbt.begin();
            for (AccountUpdate update : updates) {
                update.performUpdate(dbt);
            }
            dbt.commit();
        } catch (Exception e) {
            dbt.cancel();
            throw e;
        }
    }

    @Override
    public Account createAccount(AccountBuilder builder) throws MoneyTransferException {
        List<AccountUpdate> updates = new LinkedList<>();
        CreateAccount create = new CreateAccount(builder);
        updates.add(create);
        executeAccountUpdatesAtomically(updates);
        return create.getAccount();
    }

    // TODO
    @Override
    public Transaction createTransaction(TransactionBuilder builder) throws MoneyTransferException {
        List<AccountUpdate> updates = new LinkedList<>();
        RecordTransaction record = new RecordTransaction(builder);
        updates.add(record);
        executeAccountUpdatesAtomically(updates);
        return record.getTransaction();
    }

    private static String randomIban() {
        StringBuilder res = new StringBuilder();
        Random gen = new Random();
        int checksum = 0;
        for (int i = 0; i < 20; i++) {
            int current_val = gen.nextInt(10);
            checksum = (checksum + current_val) % 100;
            res.append(Integer.valueOf(current_val).toString());
        }
        return String.format("%02d", checksum) + res.toString();
    }

    private InMemoryTable<AccountId, Account> accounts = new InMemoryTable() {
        protected AccountId generateId() { return new AccountId(randomIban()); }
    };
    private InMemoryTable<Long, Transaction> transactions = new InMemoryTable() {
        Random random = new Random();
        protected Long generateId() { return Long.valueOf(random.nextLong());
        }
    };


    /**
     * A simple wrapper that delegates changes to each table to the corresponding InMemoryTable.StagedChanges object.
     * This way we get DB-transaction semantics - either all changes get committed, or none of them.
     */
    class DBTransaction {
        private InMemoryTable<AccountId, Account>.StagedChanges accountChanges = accounts.new StagedChanges();
        private InMemoryTable<Long, Transaction>.StagedChanges transactionChanges = transactions.new StagedChanges();

        /**
         * Make the requested account part of this DBTransaction.
         * @param id Id of the account.
         */
        void fetchAccount(AccountId id) {
            accountChanges.addId(id);
        }

        /**
         * Make the requested transaction row part of this DBTransaction.
         * @param id Id of the transaction.
         */
        void fetchTransaction(long id) {
            transactionChanges.addId(id);
        }

        /**
         * Reserve a new row id (and thus an unique id) in the Transaction table.
         */
        long reserveTransactionRow() {
            return transactionChanges.newRow();
        }

        /**
         * Reserve a new row id (and thus an unique id) in the Account table.
         */
        AccountId reserveAccountRow() {
            return accountChanges.newRow();
        }

        /**
         * Begin the DBTransaction: block all the relevant rows in tables, read their values.
         */
        void begin() {
            accountChanges.lock();
            transactionChanges.lock();
        }

        /**
         * Get the current value of the requested Account. Should only be called after begin().
         * @param accountId Identifies the account row.
         * @return The value of the account row.
         */
        public Account getAccount(AccountId accountId) {
            return accountChanges.get(accountId);
        }

        /**
         * Get the current value of the requested Transaction row. Should only be called after begin().
         * @param id Identifies the account row.
         * @return The value of the account row.
         */
        Transaction getTransaction(long id) {
            return transactionChanges.get(id);
        }

        /**
         * Set a new value for a row in account table.
         * @Param a The new value of the row.
         */
        void updateAccount(Account a) {
            accountChanges.updateRow(a.id, a);
        }

        /**
         * Set a new value for a row in transaction table.
         * @Param t The new value of the row.
         */
        void updateTransaction(Transaction t) {
            transactionChanges.updateRow(t.id, t);
        }

        /**
         * Cancel the transaction. All tables will remain unchanged.
         */
        void cancel() {
            accountChanges.release();
            transactionChanges.release();
        }

        /**
         * Commit the transaction. All changes will be applied to the tables.
         */
        void commit() {
            try {
                accountChanges.apply();
                transactionChanges.apply();
            } finally {
                accountChanges.release();
                transactionChanges.release();
            }
        }
    }

}
