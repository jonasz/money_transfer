package com.jonasz.datamodel;

import com.jonasz.exceptions.MoneyTransferException;

/**
 * Encapsulates an operation on the accounts database. Note that AccountUpdate is a lightweight, logically isolated
 * operation - it neither initiates nor commits the transaction. It is assumed that transaction management is done
 * by the caller of performUpdate.
 */
public abstract class AccountUpdate {
    public abstract void performUpdate(InMemoryDatabaseClient.DBTransaction dbt) throws MoneyTransferException;

    /**
     * Informs the DBTransaction (through calls to dbt.fetchAccount, dbt.fetchTransaction) which rows will be needed by
     * this AccountUpdate. These rows will be locked when the transaction begins.
     * @param dbt Database Transaction.
     * @throws MoneyTransferException
     */
    public abstract void fetchRows(InMemoryDatabaseClient.DBTransaction dbt) throws MoneyTransferException;
}

