package com.jonasz.datamodel;

import com.jonasz.exceptions.MoneyTransferException;

import java.util.Random;

public class CreateAccount extends AccountUpdate {
    CreateAccount(AccountBuilder builder) {
        this.builder = builder;
    }

    @Override
    public void performUpdate(InMemoryDatabaseClient.DBTransaction dbt) throws MoneyTransferException {
        builder.setFingerprint("fgpt." + String.valueOf(Math.abs(new Random().nextInt())).hashCode());
        account = builder.build();
        dbt.updateAccount(account);
    }

    @Override
    public void fetchRows(InMemoryDatabaseClient.DBTransaction dbt) throws MoneyTransferException {
        builder.setId(dbt.reserveAccountRow());
    }

    public Account getAccount() { return account; }

    private AccountBuilder builder;
    private Account account;
}
