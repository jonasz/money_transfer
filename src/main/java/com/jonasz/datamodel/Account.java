package com.jonasz.datamodel;

import com.jonasz.currency.CurrencyAmount;

/**
 * An Account instance represents a single row in the accounts table.
 */
public class Account {
    public final AccountId id;
    public final CurrencyAmount balance;
    public final String firstName;
    public final String lastName;
    public final String address;
    public final String fingerprint;

    Account(AccountId id, CurrencyAmount balance, String firstName, String lastName, String address, String fingerprint) {
        this.id = id;
        this.balance = balance;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.fingerprint = fingerprint;
    }
}

