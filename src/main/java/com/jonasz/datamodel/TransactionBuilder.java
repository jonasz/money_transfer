package com.jonasz.datamodel;

import com.jonasz.currency.CurrencyAmount;

class TransactionBuilder {
    private long id;
    private AccountId sender;
    private AccountId receiver;
    private CurrencyAmount amountSent;
    private CurrencyAmount amountReceived;
    private long timestamp;

    TransactionBuilder(Transaction other) {
        this.id = other.id;
        this.sender = other.sender;
        this.receiver = other.receiver;
        this.amountSent = other.amountSent;
        this.amountReceived = other.amountReceived;
        this.timestamp = other.timestamp;
    }

    TransactionBuilder() {}

    Transaction build() {
        return new Transaction(id, sender, receiver, amountSent, amountReceived, timestamp);
    }

    public AccountId getSender() {
        return sender;
    }

    public AccountId getReceiver() {
        return receiver;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSender(AccountId sender) {
        this.sender = sender;
    }

    public void setReceiver(AccountId receiver) {
        this.receiver = receiver;
    }

    public void setAmountSent(CurrencyAmount amountSent) {
        this.amountSent = amountSent;
    }

    public void setAmountReceived(CurrencyAmount amountReceived) {
        this.amountReceived = amountReceived;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
