package com.jonasz.datamodel;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * For now, just a wrapper around the iban number. In future, we might want to support virtual accounts with no external
 * iban, sub accounts, etc. That's when the wrapper class may prove useful.
 */
@JsonDeserialize(using = AccountIdDeserializer.class)
public class AccountId implements Comparable<AccountId> {
    private final String iban;

    public AccountId(String iban) {
        this.iban = iban;
    }
    public String getIban() {
        return this.iban;
    }

    @Override
    public int hashCode() {
        return iban.hashCode();
    }
    @Override
    public boolean equals(Object other) {
        try {
            AccountId otherId = (AccountId)other;
            return otherId.iban.equals(iban);
        } catch (Throwable t) {
            return false;
        }
    }

    @Override
    public int compareTo(AccountId other) {
        return iban.compareTo(other.iban);
    }
}
