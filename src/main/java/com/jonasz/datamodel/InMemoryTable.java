package com.jonasz.datamodel;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Concurrent in-memory table, mapping keys to values. Used by InMemoryDatabaseClient. A single database table is
 * represented by an InMemoryTable mapping from row ids to row values, for example AccountId -> Account.
 * Changes to the table are introduced atomically through an auxiliary StagedChanges object.
 * Note: the table assumes the values it keeps are not modifiable - when retrieving rows, we return references to the
 * kept values.
 * @param <IdT> Key type.
 * @param <ValT> Value type.
 */
abstract class InMemoryTable<IdT extends Comparable<IdT>, ValT> {
    private ConcurrentMap<IdT, Row> contents = new ConcurrentHashMap<>();

    private void acquireRows(Set<IdT> ids, List<ReentrantLock>acquiredLocks, Map<IdT, ValT> values) {
        // In order to avoid deadlocks, we acquire locks in order of increasing ids.
        ArrayList<IdT> sortedIds = new ArrayList<IdT>(ids);
        Collections.sort(sortedIds);
        for (IdT id : sortedIds) {
            ReentrantLock lock = contents.get(id).lock;
            lock.lock();
            values.put(id, contents.get(id).val);
            acquiredLocks.add(lock);
        }
    }


    protected abstract IdT generateId();

    /**
     * Returns the table value for the given id.
     * @param id Id of the object to fetch.
     * @return The value corresponding to the given id, or null if not found.
     */
    ValT get(IdT id) {
        // Note: the values in the table are never modified directly, only swapped for new values. Therefore, we don't
        // need a readonly lock to retrieve a value from the table.
        Row row = contents.get(id);
        if (row == null) return null;
        return row.val;
    }

    abstract class Filter {
        abstract boolean predicate(ValT val);
    }

    /**
     * Fetches all values matching the filter from the table. Note, the method is asynchronous: the result may not be
     * an atomic snapshot of the table if the table is modified concurrently. Make sure your usage matches this specification.
     * @param filter Identifies which rows should be included in the result.
     * @return A list of values that match the given filter.
     */
    List<ValT> getAllAsync(Filter filter) {
        List<ValT> result = new LinkedList<>();
        Iterator it = contents.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<IdT, Row> entry = (Map.Entry)it.next();
            Row row = entry.getValue();
            ValT val = row.val;
            if (val != null) {
                if (filter == null || filter.predicate(val)) {
                    result.add(val);
                }
            }
        }
        return result;
    }

    /**
     * Represents changes to the table. The changes are cached in StagedChanges object, and are inserted to the
     * table in a single atomic transaction. Example usage:
     * InMemoryTable<KeyT, ValT> table = (...);
     * stagedChanges = table.new StagedChanges;
     * stagedChanges.addId(id1);  # Identify all row ids involved in the transaction.
     * stagedChanges.addId(id2);
     * id3 = stagedChanges.newRow();
     * stagedChagnes.updateRow(id1, newValue1);
     * stagedChagnes.updateRow(id2, newValue2);
     * stagedChagnes.updateRow(id3, newValue3);
     */
    class StagedChanges {
        // Ids this operation works with. Rows identified by these ids will be locked for the duration of the
        // transaction.
        private Set<IdT> ids = new HashSet<IdT>();

        // Locks, from either table, that begin() acquires.
        private List<ReentrantLock> acquiredLocks = new LinkedList<ReentrantLock>();

        // We cache changes to all rows in local maps. When all operations succeed, during commit, we flush these
        // values into the database.
        private Map<IdT, ValT> cachedValues = new HashMap<IdT, ValT>();

        private boolean rowsLocked = false;

        /**
         * Identify id of a row involved in the transaction. All ids should be added before the call to lock().
         * @param id
         */
        void addId(IdT id) {
            if (rowsLocked) throw new RuntimeException("addId should not be called after lock().");
            this.ids.add(id);
        }

        /**
         * Transactions that create new rows, should allocate an id through newRow. newRow should only be called
         * before the call to lock(). There is no need to call addId() for ids obtained through newRow();
         * @return A newly allocated id.
         */
        IdT newRow() {
            // TODO: newRow allocates a row in the table for the purpose of securing an unique id, before the transaction
            // begins. Therefore, if the transaction is unrolled, the reserved row remains in the table. We can add
            // a simple mechanism to clean those ghost rows up, when the transaction is cancelled.
            if (rowsLocked) throw new RuntimeException("newRow should not be called after lock().");
            ReentrantLock lock = new ReentrantLock();
            IdT id;
            while(true) {
                id = generateId();
                Row previous_row = contents.putIfAbsent(id, new Row(lock, null));
                if (previous_row == null) {
                    break;
                }
            }
            addId(id);
            return id;
        }

        /**
         * Get the value corresponding to the given id. Should only be called after lock().
         * @param id Id of the row to get.
         * @return Value for the given id.
         */
        ValT get(IdT id) {
            if (!rowsLocked) throw new RuntimeException("get should only be called after lock().");
            return cachedValues.get(id);
        }

        /**
         * Locks all rows taking part in the transaction, and stores their value in local cache.
         * Note: after call to lock, make sure to call release() to free up the resources.
         */
        void lock() {
            if (rowsLocked) throw new RuntimeException("lock should not be called twice.");
            acquireRows(ids, acquiredLocks, cachedValues);
            rowsLocked = true;
        }

        /**
         * Introduces a change to a row. Should only be called after lock(). All changes will be committed to the
         * table on call to apply().
         * @param id Id of the row to change.
         * @param val New value of the row.
         */
        void updateRow(IdT id, ValT val) {
            if (!rowsLocked) throw new RuntimeException("updateRow should only be called after lock()");
            if (!ids.contains(id)) {
                throw new RuntimeException("Trying to modify a row that is not part of the transaction. Did you call addId?");
            }
            cachedValues.put(id, val);
        }

        /**
         * Commits all changes accumulated locally through updateRow to the table.
         */
        void apply() {
            if (!rowsLocked) throw new RuntimeException("apply should only be called after lock()");
            Iterator it = cachedValues.entrySet().iterator();

            // First: a sanity check. Note that we are guaranteed all relevant rows are in the table, and we hold locks
            // for them. Let's double check just in case; if we fail here - we have a bug somewhere.
            while (it.hasNext()) {
                Map.Entry<IdT, ValT> entry = (Map.Entry)it.next();
                Row row = contents.get(entry.getKey());
                if (row == null || row.lock == null || !row.lock.isHeldByCurrentThread()) {
                    throw new RuntimeException("Inconsistent internal state: row's lock is not held on apply().");
                }
            }

            // Second: commit all values to the table. Note, this block has to be kept simple - we don't want
            // exceptions here, as it would compromise the transaction semantics.
            it = cachedValues.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<IdT, ValT> entry = (Map.Entry)it.next();
                Row row = contents.get(entry.getKey());
                row.val = entry.getValue();
            }
        }

        /**
         * Releases the locked rows.
         */
        void release() {
            if (!rowsLocked) throw new RuntimeException("release should only be called after lock()");
            _releaseAllLocks();
        }

        private void _releaseAllLocks() {
            // Note: this has to be kept simple. We don't want exceptions here, as it could result in unreleased locks.
            for (ReentrantLock lock : acquiredLocks) {
                assert (lock.isHeldByCurrentThread());
                lock.unlock();
            }
        }
    }

    private class Row {
        public ReentrantLock lock;
        public ValT val;
        public Row(ReentrantLock lock, ValT val) {this.lock = lock; this.val = val;}
    }

}
