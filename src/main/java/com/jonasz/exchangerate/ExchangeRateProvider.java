package com.jonasz.exchangerate;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.exceptions.ExchangeRateUnknown;
import com.jonasz.exceptions.UnsupportedCurrency;

import java.math.BigDecimal;

/**
 * Provides the exchange rate to use when converting currencies. Note that exchange rates change in time - make sure
 * not to cache the results; always consult ExchangeRateProvider at the exact time of the currency exchange.
 */
public abstract class ExchangeRateProvider {
    public abstract BigDecimal getExchangeRate(String currencyIsoCodeFrom, String currencyIsoCodeTo) throws ExchangeRateUnknown;
    public CurrencyAmount convertCurrency(CurrencyAmount from, String currencyIsoCodeTo) throws UnsupportedCurrency, ExchangeRateUnknown {
        BigDecimal rate = getExchangeRate(from.getCurrencyIsoCode(), currencyIsoCodeTo);
        return CurrencyAmount.createRound(currencyIsoCodeTo, from.inUnits().multiply(rate));
    }
}
