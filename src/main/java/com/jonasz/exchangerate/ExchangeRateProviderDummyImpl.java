package com.jonasz.exchangerate;

import com.jonasz.exceptions.ExchangeRateUnknown;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Dummy implementation of an ExchangeRateProvider. The exchange rates are hardcoded and stored in a static HashMap.
 */
public class ExchangeRateProviderDummyImpl extends ExchangeRateProvider {
    private final Map<String, String> rates;

    private ExchangeRateProviderDummyImpl() {
        rates = new HashMap<>();
        setExchangeRate("USD", "CHF", ".99572");
        setExchangeRate("CHF", "USD", ".99512");
        setExchangeRate("USD", "RUB", "66.15123");
        setExchangeRate("RUB", "USD", "0.014551");
    }


    private void setExchangeRate(String currencyIsoCodeFrom, String currencyIsoCodeTo, String rate) {
        String key = constructKey(currencyIsoCodeFrom, currencyIsoCodeTo);
        rates.put(key, rate);
    }

    private static final ExchangeRateProviderDummyImpl singleton = new ExchangeRateProviderDummyImpl();
    public static ExchangeRateProviderDummyImpl getSingleton() { return singleton; }


    @Override
    public BigDecimal getExchangeRate(String currencyIsoCodeFrom, String currencyIsoCodeTo) throws ExchangeRateUnknown {
        if (currencyIsoCodeFrom.equals(currencyIsoCodeTo)) {
            return new BigDecimal("1.0");
        }
        String rate = rates.get(constructKey(currencyIsoCodeFrom, currencyIsoCodeTo));
        if (rate == null) {
            throw new ExchangeRateUnknown("Unknown rate: " + currencyIsoCodeFrom + " to " + currencyIsoCodeTo);
        }
        return new BigDecimal(rate);
    }

    private String constructKey(String currencyIsoCodeFrom, String currencyIsoCodeTo) {
        return currencyIsoCodeFrom + "_to_" + currencyIsoCodeTo;
    }
}
