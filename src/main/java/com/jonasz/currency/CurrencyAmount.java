package com.jonasz.currency;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jonasz.exceptions.IncompatibleIsoCodes;
import com.jonasz.exceptions.MalformedCurrencyAmount;
import com.jonasz.exceptions.MoneyTransferException;
import com.jonasz.exceptions.UnsupportedCurrency;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * CurrencyAmount represents a fixed amount of money in a particular currency, for example RUB 10.20, or USD -5.33.
 * CurrencyAmount is exact and can represent arbitrarily big numbers. Internally it operates on amounts expressed in
 * 'subUnits', stored in BigIntegers. For US dollar, the subUnit would be cent; for Russian Ruble, kopeyka.
 */
@JsonDeserialize(using = CurrencyAmountDeserializer.class)
@JsonSerialize(using = CurrencyAmountSerializer.class)
public class CurrencyAmount {
    public String getCurrencyIsoCode() {
        return config.currencyIsoCode;
    }
    /**
     * Constructs CurrencyAmount. The provided amount has to be exact and will not be rounded.
     * @param currencyIsoCode Identifies the currency.
     * @param amountInUnits Specifies the amount.
     * @return CurrencyAmount
     * @throws MoneyTransferException - when the provided amount contains fractional subUnits.
     */
    public static CurrencyAmount createExact(String currencyIsoCode, String amountInUnits)
            throws MoneyTransferException {
        CurrencyConfig config = CurrencyConfig.getByCurrencyIsoCode(currencyIsoCode);
        BigDecimal unitsDecimal = stringToBigDecimal(amountInUnits);
        BigInteger totalAmountInSubUnits;
        try {
            totalAmountInSubUnits = unitsDecimal.multiply(new BigDecimal(config.subUnitsInUnit)).toBigIntegerExact();
        } catch (ArithmeticException e) {
            throw new MalformedCurrencyAmount("Not a valid amount for " + currencyIsoCode + ": " + amountInUnits);
        }
        return new CurrencyAmount(config, totalAmountInSubUnits);
    }

    /**
     * Constructs CurrencyAmount. The provided amount may be rounded by discarding the fractional subUnits. So, if there
     * are 100 subUnits in a unit, * 3.128 is rounded to 3.12, -3.128 is rounded to -3.12.
     * @param currencyIsoCode Identifies the currency.
     * @param amountInUnits Specifies the amount.
     * @return CurrencyAmount
     * @throws UnsupportedCurrency When an unrecognized currencyIsoCode is provided.
     */
    public static CurrencyAmount createRound(String currencyIsoCode, BigDecimal amountInUnits) throws UnsupportedCurrency {
        CurrencyConfig config = CurrencyConfig.getByCurrencyIsoCode(currencyIsoCode);
        BigInteger totalAmountInSubUnits = amountInUnits.multiply(new BigDecimal(config.subUnitsInUnit)).toBigInteger();
        return new CurrencyAmount(config, totalAmountInSubUnits);
    }

    /**
     * Constructs CurrencyAmount. The provided amount may be rounded by discarding the fractional subUnits.
     * @param currencyIsoCode Identifies the currency.
     * @param amountInUnits Specifies the amount.
     * @return CurrencyAmount
     * @throws UnsupportedCurrency When an unrecognized currencyIsoCode is provided.
     */
    static CurrencyAmount createRound(String currencyIsoCode, String amountInUnits) throws MalformedCurrencyAmount, UnsupportedCurrency {
        return CurrencyAmount.createRound(currencyIsoCode, stringToBigDecimal(amountInUnits));
    }

    public CurrencyAmount add(CurrencyAmount other) {
        return new CurrencyAmount(this.config, totalAmountInSubUnits.add(other.totalAmountInSubUnits));
    }

    public CurrencyAmount negate() {
        return new CurrencyAmount(this.config, totalAmountInSubUnits.negate());
    }

    public String toString() {
        return config.currencyIsoCode + " " + inUnits().toString();
    }

    public BigDecimal inUnits() {
        return new BigDecimal(totalAmountInSubUnits).divide(BigDecimal.valueOf(config.subUnitsInUnit));
    }

    private final BigInteger totalAmountInSubUnits;
    private final CurrencyConfig config;

    /**
     * Constructor accepts amounts in subUnits. It is private to avoid confusion.
     * @param config Identifies the currency.
     * @param totalAmountInSubUnits Specifies the amounts in subUnits (i.e. cents or kopeykas, not dollars or rubles).
     */
    private CurrencyAmount(CurrencyConfig config, BigInteger totalAmountInSubUnits) {
        this.config = config;
        this.totalAmountInSubUnits = totalAmountInSubUnits;
    }

    static private BigDecimal stringToBigDecimal(String s) throws MalformedCurrencyAmount {
        try {
            return new BigDecimal(s);
        } catch (NumberFormatException e) {
            throw new MalformedCurrencyAmount("Couldn't convert amount to a decimal number: " + s);
        }
    }

    public int compareTo(CurrencyAmount other) throws IncompatibleIsoCodes {
        if (!other.getCurrencyIsoCode().equals(getCurrencyIsoCode())) {
            throw new IncompatibleIsoCodes(getCurrencyIsoCode() + " vs " + other.getCurrencyIsoCode());
        }
        return totalAmountInSubUnits.compareTo(other.totalAmountInSubUnits);
    }

    public String getUnits() {
        return inUnits().toString();
    }
}
