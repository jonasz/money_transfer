package com.jonasz.currency;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.jonasz.exceptions.MoneyTransferException;

import java.io.IOException;

/**
 * Implements json -> CurrencyAmount conversion.
 */
class CurrencyAmountDeserializer extends StdDeserializer<CurrencyAmount> {

    public CurrencyAmountDeserializer() {
        this(null);
    }

    public CurrencyAmountDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public CurrencyAmount deserialize(JsonParser jp, DeserializationContext ctx)
            throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String currencyIsoCode = node.get("currencyIsoCode").asText();
        String amountInUnits = node.get("amountInUnits").asText();
        try {
            return CurrencyAmount.createExact(currencyIsoCode, amountInUnits);
        } catch (MoneyTransferException e) {
            throw new IOException(e.getMessage());
        }
    }
}
