package com.jonasz.currency;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * Implements CurrencyAmount -> json conversion.
 */
class CurrencyAmountSerializer extends StdSerializer<CurrencyAmount> {

    public CurrencyAmountSerializer() {
        this(null);
    }

    public CurrencyAmountSerializer(Class<CurrencyAmount> t) {
        super(t);
    }

    @Override
    public void serialize(
            CurrencyAmount value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {

        jgen.writeStartObject();
        jgen.writeStringField("currencyIsoCode", value.getCurrencyIsoCode());
        jgen.writeStringField("amountInUnits", value.getUnits());
        jgen.writeEndObject();
    }
}
