package com.jonasz.currency;

import com.jonasz.exceptions.UnsupportedCurrency;

/**
 * A Simple class that encapsulates information about a particular currency.
 */
public class CurrencyConfig {
    public final String currencyIsoCode;
    public final int subUnitsInUnit;

    private CurrencyConfig(String currencyIsoCode, int subUnitsInUnit) {
        this.currencyIsoCode = currencyIsoCode;
        this.subUnitsInUnit = subUnitsInUnit;
    }

    static public final CurrencyConfig[] knownCurrencies = {
        new CurrencyConfig("CHF", 100),
        new CurrencyConfig("USD", 100),
        new CurrencyConfig("RUB", 100),
        new CurrencyConfig("PLN", 100),
        new CurrencyConfig("CZK", 1),
        new CurrencyConfig("KWD", 1000),
    };

    public static CurrencyConfig getByCurrencyIsoCode(String currencyIsoCode) throws UnsupportedCurrency {
        for (CurrencyConfig config : knownCurrencies) {
            if (config.currencyIsoCode.equals(currencyIsoCode)) {
                return config;
            }
        }
        throw new UnsupportedCurrency(currencyIsoCode);
    }
}
