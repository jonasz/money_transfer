package com.jonasz.exceptions;

public class ExchangeRateUnknown extends MoneyTransferException {
    public ExchangeRateUnknown(String s) { super(s); }
}
