package com.jonasz.exceptions;

/**
 * Base exception for all exceptions thrown by MoneyTransferService.
 */
public class MoneyTransferException extends Exception {
    public MoneyTransferException(String msg) {
        super(msg);
    }
}

