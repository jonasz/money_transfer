package com.jonasz.exceptions;

class InconsistentOperation extends MoneyTransferException {
    public InconsistentOperation(String s) { super(s); }
}
