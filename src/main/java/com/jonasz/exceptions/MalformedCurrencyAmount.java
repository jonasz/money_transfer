package com.jonasz.exceptions;

public class MalformedCurrencyAmount extends MoneyTransferException {
    public MalformedCurrencyAmount(String s) {
        super(s);
    }
}
