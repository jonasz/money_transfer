package com.jonasz.exceptions;

public class IncompatibleIsoCodes extends MoneyTransferException {
    public IncompatibleIsoCodes(String s) { super(s); }
}
