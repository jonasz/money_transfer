package com.jonasz.exceptions;

public class UnsupportedCurrency extends MoneyTransferException {
    public UnsupportedCurrency(String currencyIsoCode) {
        super("Unsupported ISO code: " + currencyIsoCode);
    }
}
