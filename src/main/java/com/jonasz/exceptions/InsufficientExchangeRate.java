package com.jonasz.exceptions;

public class InsufficientExchangeRate extends MoneyTransferException {
    public InsufficientExchangeRate(String s) { super(s); }
}
