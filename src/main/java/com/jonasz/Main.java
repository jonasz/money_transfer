package com.jonasz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jonasz.datamodel.AccountId;
import com.jonasz.datamodel.DatabaseClient;
import com.jonasz.datamodel.InMemoryDatabaseClient;
import com.jonasz.exceptions.MoneyTransferException;
import com.jonasz.exchangerate.ExchangeRateProvider;
import com.jonasz.exchangerate.ExchangeRateProviderDummyImpl;
import com.jonasz.internalaccounting.InternalAccountingProvider;
import com.jonasz.internalaccounting.InternalAccountingProviderImpl;
import com.jonasz.service.*;
import spark.Request;
import spark.Response;

import static spark.Spark.*;


/**
 * Initializes all dependencies of MoneyTransferService, and exposes the service over HTTP.
 */
public class Main {
    private static String objToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        return objectMapper.writeValueAsString(obj);
    }

    private static MoneyTransferService createMoneyTransferService() throws MoneyTransferException {
        DatabaseClient database = InMemoryDatabaseClient.getSingleton();
            InternalAccountingProviderImpl.InitializeSingleton(database);
        InternalAccountingProvider internalAccountingProvider = InternalAccountingProviderImpl.getSingleton();
        ExchangeRateProvider exchangeRateProvider = ExchangeRateProviderDummyImpl.getSingleton();
        return  new MoneyTransferServiceImpl(database, internalAccountingProvider, exchangeRateProvider);
    }

    public static void main(String[] args) {
        MoneyTransferService moneyTransferService;
        try {
            moneyTransferService = createMoneyTransferService();
        } catch (MoneyTransferException e) {
            e.printStackTrace();
            System.exit(1);
            return;
        }

        get("/account", (Request request, Response response) -> {
            String iban = request.queryParams("iban");
            if (iban == null) {
                response.status(400);
                return "";
            }
            GetAccountRequest req = new GetAccountRequest(new AccountId(iban));
            return objToJson(moneyTransferService.getAccount(req));
        });

        post("/account", (Request request, Response response) -> {
            CreateAccountRequest req;
            try {
                req = new ObjectMapper().readValue(request.body(), CreateAccountRequest.class);
            } catch (JsonProcessingException e) {
                response.status(400);
                return e.getMessage();
            }
            return objToJson(moneyTransferService.createAccount(req));
        });

        get("/history", (Request request, Response response) -> {
            String iban = request.queryParams("iban");
            if (iban == null) {
                response.status(400);
                return "";
            }
            AccountHistoryResponse res;
            AccountHistoryRequest req = new AccountHistoryRequest(new AccountId(iban));
            return objToJson(moneyTransferService.getAccountHistory(req));
        });

        post("/transfer", (Request request, Response response) -> {
            TransferMoneyRequest req;
            try {
                req = new ObjectMapper().readValue(request.body(), TransferMoneyRequest.class);
            } catch (JsonProcessingException e) {
                response.status(400);
                return e.getMessage();
            }
            return objToJson(moneyTransferService.transferMoney(req));
        });

        System.out.println("Server running at localhost:4567.");
    }
}
