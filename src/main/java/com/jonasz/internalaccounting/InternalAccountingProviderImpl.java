package com.jonasz.internalaccounting;

import com.jonasz.currency.CurrencyAmount;
import com.jonasz.currency.CurrencyConfig;
import com.jonasz.datamodel.Account;
import com.jonasz.datamodel.AccountBuilder;
import com.jonasz.datamodel.AccountId;
import com.jonasz.datamodel.DatabaseClient;
import com.jonasz.exceptions.MoneyTransferException;

import java.util.HashMap;
import java.util.Map;

/**
 * InternalAccountingProvider that creates all relevant internal accounts upon initialization.
 */
public class InternalAccountingProviderImpl implements InternalAccountingProvider {
    private final Map<String, AccountId> accounts;
    private final DatabaseClient database;

    private void setUpAccount(CurrencyAmount initialBalance) throws MoneyTransferException {
        AccountBuilder builder = new AccountBuilder();
        builder.setBalance(initialBalance);
        builder.setFirstName("Jonasz");
        builder.setLastName("Pamula");
        builder.setAddress("Times Square 1, NYC, USA");

        Account account = database.createAccount(builder);
        accounts.put(initialBalance.getCurrencyIsoCode(), account.id);
    }
    private static InternalAccountingProviderImpl singleton;
    public static void InitializeSingleton(DatabaseClient databaseClient) throws MoneyTransferException {
        singleton = new InternalAccountingProviderImpl(databaseClient);
    }
    public static InternalAccountingProviderImpl getSingleton() { return singleton; }

    private InternalAccountingProviderImpl(DatabaseClient database) throws MoneyTransferException {
        this.database = database;
        accounts = new HashMap<>();
        for (CurrencyConfig currencyConfig : CurrencyConfig.knownCurrencies) {
            setUpAccount(CurrencyAmount.createExact(currencyConfig.currencyIsoCode, "0"));
        }
    }


    @Override
    public AccountId getInternalAccount(String currencyIsoCode) {
        return accounts.get(currencyIsoCode);
    }
}
