package com.jonasz.internalaccounting;

import com.jonasz.datamodel.AccountId;

/**
 * Knows how to access internal accounts used for currency accounting. During a money transfer, when performing a
 * currency exchange, we buy the sender's currency and sell the receiver's currency. These transactions are summarized
 * in the internal accounts - so at the end of the day, we know how much of each currency we have sold or bought in total.
 */
public interface InternalAccountingProvider {
    /**
     * @param currencyIsoCode Identifies the currency of the account.
     * @return The id of the internal account with the specified currency.
     */
    AccountId getInternalAccount(String currencyIsoCode);
}

